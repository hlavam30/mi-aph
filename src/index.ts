import { loader } from 'pixi.js'
import * as WebFont from 'webfontloader'
import 'pixi-sound'

import Application from './engine/application'
import Scene from './engine/scene'
import IntroScene from './introScene'
import LoadScene from './loadScene'
import SoundManager from './soundManager'
import OutroScene from './outroScene';

const assetsCatalog = require('../assets/assets-catalog.json')
const config = require('../config.json')

type AssetCatalogItem = { name: string, path: string }

const canvas = <HTMLCanvasElement>document.getElementById(config.canvasElementId)

// PIXI settings
PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST

// Creates an application on #application canvas element
const app = new Application(canvas, config, null)

// Display the load scene
const loadScene = new LoadScene(app)
app.attachScene(loadScene)

// Load webfonts, then...
WebFont.load({
    google: { families: ['Press Start 2P'] },
    active: loadAssets,
})

// ... load all assets from repository, finally...
function loadAssets () {
    loader.reset()
    assetsCatalog.forEach((asset: AssetCatalogItem) => {
        loader.add(asset.name, asset.path)
    })
    loader.load(onLoaded)
    loader.onProgress.add(p => loadScene.updateProgress(p.progress))
}

// ... initialize the game.
function onLoaded (_loader, _resources) {
    console.log('Assets loaded, game setting up...')
    SoundManager.setUp()
    app.attachScene(new IntroScene(app), true)
}

