import Scene from './engine/scene'
import { Text, Graphics, Sprite } from './engine/gameobject'
import Application from './engine/application'
import KeyInputComponent, { KeyboardKey } from './components/keyInput'
import MainScene from './mainScene'
import GameCore from './components/gameCore'
import { MessageAction } from './engine/message'
import SoundManager from './soundManager'

/**
 * Intro scene is displayed before the game starts. It asks for username.
 */
export default class LoadScene extends Scene {
    private progress: Graphics
    private inputText: Text

    studentName: string = ''

    constructor(app: Application) {
        super()
        this.application = app
        const { width, height } = app.pixi.screen
        const prompt = 'DEAR STUDENT, ENTER YOUR CTU USERNAME:\n\n'

        // Create keyinput component
        const keyInput = new KeyInputComponent(this.stage)
        this.addGlobalComponent(keyInput)

        // Draw logo
        const logo = new Sprite(PIXI.Texture.from('logo'), 'logo', this)
        logo.anchor.set(0.5, 0.5)
        logo.position.set(width / 2, height / 4)
        this.addObject(logo)

        // Draw the main instructions text
        const wText = new Text(
            'USE [W][A][S][D] FOR MOVING, [K] FOR FIRE\n\n\n\n' +
            'SHOOT ALL NONSENSE STATEMENTS, \nDON\'T SHOOT THE CORRECT ONES.' +
            '\n\n\n\nPRESS [ENTER] TO START' +
            '\n\n\n\nMUSIC BY DDLBLUE        CHARACTERS GFX BY VARENEZELI' +
                '\n\n(c) 2019 MARIAN HLAVAC (HLAVAM30), FIT CTU IN PRAGUE', 
            'wtext', this, 
            { fill: 'white', font: '12px Press Start 2P', align: 'center' }
        )
        this.addObject(wText)
        wText.anchor.set(0.5, 0.5)
        wText.position.set(width / 2, height * 4/5)

        // Draw username input prompt text
        this.inputText = new Text(
            prompt + '_', 
            'atext', this, 
            { fill: 'white', font: '18px Press Start 2P', align: 'center' }
        )
        this.addObject(this.inputText)
        this.inputText.anchor.set(0.5, 0.5)
        this.inputText.position.set(width / 2, height / 2)

        // Listen to key presses
        keyInput.setAnyListener((key) => {
            if (key[0] && key.length == 1 && this.studentName.length < 8) {
                this.studentName += key[0]
            }

            if (key == 'Backspace') {
                this.studentName = this.studentName.slice(null, -1)
            }

            this.inputText.text = prompt + this.studentName + '_'

            // Advance the intro screen when Enter is pressed
            if (key == 'Enter') {
                SoundManager.blip.play()
                if (this.studentName.length == 0) {
                    this.inputText.text = prompt + this.studentName + '_\n\nComeoooon, it\'s just an username.'
                    return
                }

                if (this.studentName.toLowerCase().startsWith('vesec') || this.studentName.toLowerCase().startsWith('svecad')) {
                    this.inputText.text = prompt + this.studentName + '_\n\nThaaaaat is not a student\'s name.\nBuuuut you can play as me (hlavam30).'
                    return
                }
                const mainScene = new MainScene(app, sc => {
                        const gc = <GameCore>sc.findGlobalComponentByClass('GameCore')
                        gc.state.studentName = this.studentName
                        gc.sendMessage(MessageAction.STATE_CHANGED)
                })
                app.attachScene(mainScene, true)
                SoundManager.introMusic.stop()
            }
        })

        // Play some tunes
        SoundManager.introMusic.play()
    }
}