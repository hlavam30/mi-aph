import GameObject, { Sprite, nextIdWillBe, Container, Text } from './engine/gameobject'
import * as PIXI from 'pixi.js'
import { Claim } from './claimLoader'
import Scene from './engine/scene'
import { EasingFunctions } from './engine/utils'

import BulletComponent from './components/bullet'
import PlayerControllableComponent from './components/playerControllable'
import KeyInputComponent from './components/keyInput'
import TeacherBehaviourComponent from './components/teacherBehaviour'
import ClaimComponent from './components/claim'
import ParticleEmitter from './components/particleEmitter'
import HitDisplayComponent from './components/hitDisplay'

/**
 * Creates a player object.
 * @param scene Scene to be object created in
 */
export function makePlayer (scene: Scene) : GameObject {
    const player = new Sprite(PIXI.Texture.from('player'), 'player', scene)
    scene.addObject(player)

    const playerInput = new KeyInputComponent(player)
    player.addComponent(playerInput)
    player.addComponent(new PlayerControllableComponent(playerInput, player))
    player.anchor.set(0.5,0.5)  
    player.position.set(480, 480)
    
    return player
}

/**
 * Creates a teacher object.
 * @param scene Scene to be object created in
 */
export function makeTeacher (scene: Scene) : GameObject {
    const teacher = new Sprite(PIXI.Texture.from('teacher'), 'teacher', scene)
    scene.addObject(teacher)

    teacher.addComponent(new TeacherBehaviourComponent(teacher))
    teacher.position.set(480, 240)
    teacher.anchor.set(0.5,0.5)  
    
    return teacher
}

/**
 * Creates a random claim object.
 * @param text 
 * @param correct 
 * @param scene Scene to be object created in
 */
export function makeClaim (at: PIXI.Point, claim: Claim, speed: number, scene: Scene) : GameObject {
    const claimObj = new Sprite(PIXI.Texture.from('note'), 'claim', scene)
    scene.addObject(claimObj)

    claimObj.position = at
    claimObj.anchor.set(0.5,0.5)   
    claimObj.addComponent(new ClaimComponent(speed, claim, claimObj))
    
    return claimObj
}

/**
 * Creates a bullet.
 * @param origin Origin position to create bullet at
 * @param speed Vertical speed of the bullet
 * @param scene Scene to be object created in
 */
export function makeBullet (origin: PIXI.Point, speed: number, scene: Scene) : GameObject {
    const bullet = new Sprite(PIXI.Texture.from('bullet'), 'bullet', scene)
    scene.addObject(bullet)

    bullet.addComponent(new BulletComponent(speed, bullet))
    bullet.position = origin
    bullet.anchor.set(0.5,0.5)  
    return bullet
}

/**
 * Creates a poof effect after destroying a note object.
 * @param origin Where to create the effect
 * @param scene Scene to be object created in
 */
export function makePoofEffect (origin: PIXI.Point, scene: Scene) : GameObject {
    const emitter = new Container('poof-effect', scene)
    emitter.position = origin
    scene.addObject(emitter)

    const pemitter = new ParticleEmitter({
        angle: { from: 0, to: 360 },
        speed: { from: 0.3, to: 1 },
        rotation: { from: 0.1, to: 0.3 },
        texture: ['debris1', 'debris2', 'debris3'],
        lifetime: { from: 10, to: 60 },
        easing: EasingFunctions.PreciseEaseOut,
    }, emitter, true)
    emitter.addComponent(pemitter)

    pemitter.burst(15)

    return emitter
}

/**
 * Creates a small display of text specified.
 * @param text Text to be displayed
 * @param at Where to create the text
 * @param scene Scene to be object created in
 */
export function makeHitDisplay (text: string, at: PIXI.Point, scene: Scene) : GameObject {
    const textObj = new Text(text, 'hittext', scene, {
        font: '12px Press Start 2P', fill: 'white' })
    textObj.anchor.set(0.5, 1)
    textObj.position = at
    scene.addObject(textObj)

    textObj.addComponent(new HitDisplayComponent(1000, at, textObj))

    return textObj
}