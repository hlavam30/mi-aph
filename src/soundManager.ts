import * as PIXI from 'pixi.js'
import 'pixi-sound'

class ManagedSound {
    resource: PIXI.loaders.Resource 
    isPlaying: boolean = false

    constructor (resource: PIXI.loaders.Resource) {
        this.resource = resource
    }

    play(force = false) {
        if (!this.isPlaying || force) {
            (<any>this.resource).sound.play(null, {
                complete: (ev) => { console.log(ev) ;this.isPlaying = false }
            })
            this.isPlaying = true
        }
    }

    loop() {
        if (!this.isPlaying) {
            (<any>this.resource).sound.play(null, {
                complete: () => { this.isPlaying = false },
                loop: true
            })
            this.isPlaying = true
        }
    }

    stop() {
        if (this.isPlaying) {
            (<any>this.resource).sound.stop()
            this.isPlaying = false
        }
    }
}

export default class SoundManager {
    static introMusic: ManagedSound
    static gameMusic: ManagedSound
    static blip: ManagedSound
    static shoot: ManagedSound
    static correct: ManagedSound
    static mistake: ManagedSound

    static setUp () {
        this.introMusic = new ManagedSound(PIXI.loader.resources.introMusic)
        this.gameMusic = new ManagedSound(PIXI.loader.resources.gameMusic)
        this.blip = new ManagedSound(PIXI.loader.resources.blip)
        this.shoot = new ManagedSound(PIXI.loader.resources.shoot)
        this.correct = new ManagedSound(PIXI.loader.resources.correct)
        this.mistake = new ManagedSound(PIXI.loader.resources.mistake)
    }
}