import Component from '../engine/component'
import Message, { MessageAction } from '../engine/message'
import { EasingFunctions } from '../engine/utils'
import { Text } from '../engine/gameobject'

const moveBy = 120 //px

/**
 * Component for single display of score change or other temporary text.
 * Moves itself by `moveBy` pixels and then destroys itself.
 */
export default class HitDisplayComponent extends Component {
    private origin: PIXI.Point
    private started = null
    private lifetime: number

    constructor (lifetime: number, at: PIXI.Point, owner) {
        super(owner)
        this.lifetime = lifetime
        this.origin = at
    }

    onUpdate (deltaTime: number, elapsedTime: number) {
        if (!this.started) this.started = elapsedTime
        const t = elapsedTime - this.started

        this.owner.y = this.origin.y - (EasingFunctions.EaseOut(t / this.lifetime) * moveBy)

        if (t > this.lifetime) {
            this.owner.destroyObject()
        }
    }
}