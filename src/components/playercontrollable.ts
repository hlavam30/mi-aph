import Component from '../engine/component'
import KeyInputComponent, { KeyboardKey } from './keyInput'
import Message, { MessageAction } from '../engine/message'
import { Dynamics, Vec2, limitPositionToBoundaries } from '../engine/utils'
import { makeBullet } from '../prefabFactory'
import SoundManager from '../soundManager'

/**
 * Component allowing player to control a character.
 */
export default class PlayerControllableComponent extends Component {
    private keyinput: KeyInputComponent = null
    private dynamics: Dynamics
    private appBoundaries: PIXI.Rectangle
    moveSpeed = 2
    maxSpeed = 75

    constructor (keyinput: KeyInputComponent, owner) {
        super(owner)
        this.keyinput = keyinput
        this.dynamics = new Dynamics(Vec2.zero(), 1.1)
    }

    onInit () {
        // Bullet fire
        this.keyinput.setListener(KeyboardKey.FIRE, this.fireBullet.bind(this))
        this.appBoundaries = this.owner.scene.application.pixi.screen
    }

    getMoveVector () {
        let move = Vec2.zero()

        if (this.keyinput.isKeyPressed(KeyboardKey.LEFT)) {
            move = move.add(new Vec2(-1, 0))
        }
        if (this.keyinput.isKeyPressed(KeyboardKey.RIGHT)) {
            move = move.add(new Vec2(1, 0))
        }
        if (this.keyinput.isKeyPressed(KeyboardKey.UP)) {
            move = move.add(new Vec2(0, -1))
        }
        if (this.keyinput.isKeyPressed(KeyboardKey.DOWN)) {
            move = move.add(new Vec2(0, 1))
        }

        return move.limit(1)
    }

    fireBullet () {
        const bulletSpeed = 6.0 - (this.dynamics.velocity.y < 0 ? this.dynamics.velocity.y * 0.5 : 0)
        const bullet = makeBullet(this.owner.position, bulletSpeed, this.owner.scene)
        SoundManager.shoot.play(true)
        this.sendMessage(MessageAction.BULLETS, [ 'created', bullet ])
    }

    onUpdate (deltaTime: number, elapsedTime: number) {
        // Character moving
        this.dynamics.accelerate(this.getMoveVector().multiply(this.moveSpeed))
        this.dynamics.velocity.limit(this.maxSpeed)
        this.dynamics.applyFriction(deltaTime)
        this.owner.moveByVec(this.dynamics.velocity.multiply(deltaTime))
        limitPositionToBoundaries(this.owner.position, this.appBoundaries, new PIXI.Point(20, 20))
    }
}