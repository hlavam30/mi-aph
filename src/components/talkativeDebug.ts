import Component from '../engine/component'
import Message, { MessageAction } from '../engine/message'

/**
 * Talkative debug component is used for debugging purposes - mostly for 
 * determining if the messaging system works.
 * 
 * In the final version of the game this component is not used.
 */
export default class TalkativeDebugComponent extends Component {
    onInit () {
        console.log(`Component ${this.id} has been initialized.`)
        this.subscribe(MessageAction.ANY)
        this.sendMessage(MessageAction.DEBUG, 'hai')
    }

    onMessage (message: Message) {
        console.log(`Component ${this.id} has been messaged: 
        ACTION ${message.action}.
        SENDER ${message.sender.id}.
        DATA ${message.data}.`)
    }

    onUpdate (deltaTime: number, elapsedTime: number) {
        // console.log(`Component ${this.id} has been updated dt=${deltaTime}, 
        // running for ${elapsedTime} (${Math.round(elapsedTime/100)/10} seconds).`)
    }

    onRemove () {
        console.log(`Component ${this.id} has been removed.`)
    }
}