import Component from '../engine/component'

export enum KeyboardKey {
    LEFT = 65,
    UP = 87,
    RIGHT = 68,
    DOWN = 83,
    FIRE = 75,
    SELECT = 76,
}

/**
 * Component for key-input handling
 */
export default class KeyInputComponent extends Component {

    private pressedKeys = new Set<KeyboardKey>()
    private handlers = new Map<KeyboardKey, Function>()
    private listeners = new Set<(number) => any>()

    private wEventListeners = {
        'keyup': this.onKeyUp.bind(this),
        'keydown': this.onKeyDown.bind(this),
        'blur': this.onFocusOut.bind(this),
    }

    onInit () {
        window.addEventListener('keyup', this.wEventListeners['keyup'], false)
        window.addEventListener('keydown', this.wEventListeners['keydown'], false)
        window.addEventListener('blur', this.wEventListeners['blur'], false)
    }

    onRemove () {
        window.removeEventListener('keyup', this.wEventListeners['keyup'])
        window.removeEventListener('keydown', this.wEventListeners['keydown'])
        window.removeEventListener('blur', this.wEventListeners['blur'])
    }

    /**
     * Checks if certain key is currently pressed.
     * @param key A keyboard key to be checked
     */
    isKeyPressed (key: KeyboardKey) {
        return this.pressedKeys.has(key)
    }

    /**
     * Calls a specified function when certain key is pressed.
     * @param key A keyboard key
     * @param func Function to be called
     */
    setListener (key: KeyboardKey, func: Function) {
        this.handlers.set(key, func)
    }

    /**
     * Calls a specified function when any key is pressed. Pressed key is
     * handed over as parameter of the function.
     * @param func Function to be called
     */
    setAnyListener (func: (number) => any) {
        this.listeners.add(func)
    }

    /**
     * Removes any listening function to any key press.
     * @param func Function reference
     */
    removeAnyListener (func: (number) => any) {
        this.listeners.delete(func)
    }

    private onKeyDown (ev: KeyboardEvent) {
        this.pressedKeys.add(ev.keyCode)

        if (this.handlers.has(ev.keyCode)) {
            this.handlers.get(ev.keyCode)()
        }

        this.listeners.forEach(l => l(ev.key))
    }

    private onKeyUp (ev: KeyboardEvent) {
        this.pressedKeys.delete(ev.keyCode)
    }

    private onFocusOut (_ev) {
        this.pressedKeys.clear()
    }
}