import Component from '../engine/component'
import Message, { MessageAction } from '../engine/message'
import { angleLerp, Vec2, Dynamics, limitPositionToBoundaries } from '../engine/utils'
import { makeClaim } from '../prefabFactory'
import ClaimLoader from '../claimLoader'
import GameCore from './gameCore'
import GameObject from '../engine/gameobject'
import Timer, { TimedEvent } from '../engine/timer'

/**
 * Component defining the behaviour of teacher charater. It handles its moves,
 * throws down the statemens for player to shoot and so on.
 */
export default class TeacherBehaviourComponent extends Component {
    dynamics: Dynamics
    wanderTarget: Vec2
    timer = new Timer()

    private maxTargetDelay = 7000
    private targetTimer: TimedEvent

    private claimLoader = new ClaimLoader()
    private gameCore: GameCore
    private appBoundaries: PIXI.Rectangle

    constructor (owner) {
        super(owner)
        this.dynamics = new Dynamics(Vec2.zero(), 1.15)
        this.wanderTarget = new Vec2(480, 0)
    }

    onInit () {
        this.targetTimer = this.timer.time(this.changeTarget.bind(this), this.maxTargetDelay, true)
        this.gameCore = <GameCore>this.owner.scene.findGlobalComponentByClass('GameCore')
        this.appBoundaries = this.owner.scene.application.pixi.screen

        this.subscribe(MessageAction.DIFFICULTY_CHANGED)
    }

    onMessage (msg: Message) {
        if (msg.action == MessageAction.DIFFICULTY_CHANGED) {
            const newDelay = this.maxTargetDelay / this.gameCore.state.difficulty
            this.targetTimer.interval = newDelay
        }
    }

    changeTarget () {
        this.wanderTarget = new Vec2(Math.random() * 560 + 200, Math.random() * 90 + 100)
        this.throwClaim()
    }

    throwClaim () {
        const claim = this.claimLoader.getRandomClaim()
        const speed = 0.25 + this.gameCore.state.difficulty / 3
        makeClaim(this.owner.position, claim, speed, this.owner.scene)
    }

    /**
     * Seeks target, slowly turning around to reach it.
     * @param target Target to be seeked.
     * @param position Current position
     * @param currentVelocity Current velocity
     * @param velocity Moving velocity
     * @param slowingRadius Slowing radius
     * @param turningSpeed Turning around speed (0.0-1.0)
     */
    seekSlow (target: Vec2, position: Vec2, currentVelocity: Vec2, velocity: number, slowingRadius: number, turningSpeed: number) {
        const desired = target.subtract(position).normalize()
        const distance = target.subtract(position).magnitude()

        const desiredAngle = Math.atan2(desired.y, desired.x)
        const currentAngle = Math.atan2(currentVelocity.y, currentVelocity.x)

        const angle = angleLerp(currentAngle, desiredAngle, turningSpeed)
        const force = new Vec2(Math.cos(angle), Math.sin(angle))

        return force.multiply(distance < slowingRadius ? velocity * distance / slowingRadius : velocity)
    }

    /**
     * Turns around and dodges specified point.
     * @param from Point to flee from
     * @param position Current position
     * @param currentVelocity Current velocity
     * @param velocity Moving velocity (desired)
     * @param fleeRadius Radius of running away from the point
     * @param turningSpeed Turning around speed (0.0-1.0)
     */
    dodge (from: Vec2, position: Vec2, currentVelocity: Vec2, velocity: number, fleeRadius: number, turningSpeed: number) {
        const distance = from.subtract(position).magnitude()
        const fleeVelocity = distance < fleeRadius ? Math.log(fleeRadius - distance) * velocity * 0.1 : 0
        const force = this.seekSlow(position, from, currentVelocity, fleeVelocity, 0, turningSpeed)

        return force
    }

    /**
     * Chooses a closes bullet in the scene.
     */
    chooseClosestBullet () : GameObject {
        const bullets = this.owner.scene.findObjectsByTag('bullet')

        if (bullets.length == 0) {
            return null
        }
        
        const myPos = new Vec2(this.owner.x, this.owner.y)
        const distanceFromMe = (x: number, y: number) => new Vec2(x, y).subtract(myPos).magnitude()

        bullets.sort((a, b) => distanceFromMe(a.x, a.y) - distanceFromMe(b.x, b.y))

        return bullets[0]
    }

    onUpdate (deltaTime: number, elapsedTime: number) {
        this.timer.update(elapsedTime)

        const position = Vec2.fromPixiPoint(this.owner.position)
        const movingSpeed = 0.5 + this.gameCore.state.difficulty / 3
        const turningSpeed = 0.2 + this.gameCore.state.difficulty / 10

        // Go to current target
        let force = this.seekSlow(
            this.wanderTarget, 
            position, 
            this.dynamics.velocity, 
            movingSpeed, 
            200, turningSpeed < 0.9 ? turningSpeed : 0.9
        )

        // Dodge the closes bullet in the scene.
        const closestBullet = this.chooseClosestBullet()

        if (closestBullet) {
            const dodgeForce = this.dodge(
                new Vec2(closestBullet.x, closestBullet.y), 
                position, 
                this.dynamics.velocity, 3, 100, 0.6)

            force = force.add(dodgeForce)
        }

        // Add those forces together and apply them.
        this.dynamics.accelerate(force)
        this.dynamics.velocity.limit(5)
        this.dynamics.applyFriction(deltaTime)
        this.owner.moveByVec(this.dynamics.velocity.multiply(deltaTime))
        limitPositionToBoundaries(this.owner.position, this.appBoundaries, new PIXI.Point(20, 20))
    }
}