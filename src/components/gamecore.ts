import Component from '../engine/component'
import Message, { MessageAction } from '../engine/message'
import OutroScene from '../outroScene'
import Timer from '../engine/timer'
import SoundManager from '../soundManager'

interface GameState {
    score: number
    lives: number
    difficulty: number
    studentName: string
}

/**
 * GameCore component is the main component, meant to be used as
 * global component, cotaining the game state.
 */
export default class GameCore extends Component {
    state: GameState
    timer = new Timer()
    
    private difficultyRaiseSpeed = 0.1

    constructor (owner) {
        super(owner)

        this.state = {
            score: 0,
            lives: 5,
            difficulty: 1,
            studentName: '',
        }
    }

    onInit () {
        this.subscribe(MessageAction.SCORE, MessageAction.LIVES)

        // Raise difficulty each 10 seconds
        this.timer.time(this.raiseDifficulty.bind(this), 10000, true)
    }

    onMessage (msg: Message) {
        // When player scored...
        if (msg.action == MessageAction.SCORE) {
            SoundManager.correct.play(true)
            this.state.score += msg.data
            this.sendMessage(MessageAction.STATE_CHANGED)
        }

        // When player lost a live...
        if (msg.action == MessageAction.LIVES) {
            SoundManager.mistake.play(true)
            this.state.lives += msg.data
            this.sendMessage(MessageAction.STATE_CHANGED)
        }

        // Check losing conditions
        if (this.state.lives <= 0) {
            this.sendMessage(MessageAction.GAME_OVER)
            this.gameOver()
        }
    }

    onUpdate (deltaTime: number, elapsedTime: number) {
        this.timer.update(elapsedTime)
    }

    raiseDifficulty () {
        this.state.difficulty += this.difficultyRaiseSpeed
        this.sendMessage(MessageAction.DIFFICULTY_CHANGED, this.state.difficulty)
    }

    gameOver () {
        const outroScene = new OutroScene(this.owner.scene.application)
        this.owner.scene.application.attachScene(outroScene)
    }
}