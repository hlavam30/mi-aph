import Component from '../engine/component'
import Message, { MessageAction } from '../engine/message'
import GameObject, { Text } from '../engine/gameobject'
import { Claim } from '../claimLoader'
import { chunk } from '../engine/utils'
import { makePoofEffect, makeHitDisplay } from '../prefabFactory'

/**
 * Component for managing single falling claim/statement on screen,
 * defining the behaviour when hit or when fallen outside the viewport.
 */
export default class ClaimComponent extends Component {
    private speed: number
    private claim: Claim
    private worth: number = 500

    constructor (speed: number, claim: Claim, owner) {
        super(owner)
        this.speed = speed
        this.claim = claim
    }

    onInit () {
        const lines = this.splitTextIntoLinesArray(this.claim.text, 4)

        const text = new Text(
            lines.join('\n'), 
            'claim' + this.claim.id, null, 
            { fill: 'white', font: '8px Press Start 2P', align: 'center', lineHeight: 14 }
        )
        
        text.anchor.set(0.5, 1)
        text.position.set(0, -24)

        this.owner.addChildGameObject(text)
    }

    /**
     * Splits the text into more lines, allowing certain number of words
     * per line.
     * @param text Text
     * @param maxWords Maximum words per line
     */
    splitTextIntoLinesArray (text: string, maxWords: number) : string[] {
        const words = text.split(' ')
        return chunk(words, maxWords).map(ch => ch.join(' '))
    }

    /**
     * Checks if the objects is colliding with any bullet in the scene.
     */
    isCollidingBullet () : GameObject | null {
        let collision = null

        for (const bullet of this.owner.scene.findObjectsByTag('bullet')) {
            if (this.owner.checkCircleCollisionWith(bullet, 24)) {
                collision = bullet
                break
            }
        }

        return collision
    }

    onUpdate (deltaTime: number, elapsedTime: number) {
        // Let it fall
        this.owner.y += this.speed * deltaTime
        this.worth = this.worth > 100 ? this.worth - deltaTime : 100

        // Destroy object when outside of view
        const app = this.owner.scene.application
        if (this.owner.y > app.pixi.screen.height) {
            if (this.claim.correct) {
                this.sendMessage(MessageAction.SCORE, 200)
                makeHitDisplay('+200', this.owner.position, this.owner.scene)
            } else {
                this.sendMessage(MessageAction.LIVES, -1)
                makeHitDisplay('OOPS', this.owner.position, this.owner.scene)
            }

            this.owner.destroyObject()
            return
        }

        // Check collisions with bullets
        const collider = this.isCollidingBullet()
        if (collider) {
            this.sendMessage(MessageAction.BULLET_HIT)
            if (this.claim.correct) {
                this.sendMessage(MessageAction.LIVES, -1)
                makeHitDisplay('OOPS', this.owner.position, this.owner.scene)
            } else {
                const score = Math.round(this.worth / 10) * 10
                this.sendMessage(MessageAction.SCORE, score)
                makeHitDisplay('+' + score, this.owner.position, this.owner.scene)
            }

            makePoofEffect(this.owner.position, this.owner.scene)

            this.owner.destroyObject()
            collider.destroyObject()
        }
    }
}