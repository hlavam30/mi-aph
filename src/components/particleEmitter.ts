import Component from '../engine/component'
import * as PIXI from 'pixi.js'
import { Vec2, EasingFunction, EasingFunctions } from '../engine/utils'
import { Container } from '../engine/gameobject'
import { isNumber, isArray, isString } from 'util'

interface DynamicParticle {
    object: PIXI.DisplayObject
    velocity: Vec2
    rotationalVelocity: number
    t: number
    lifetime: number
    easing: EasingFunction
}

type PropertyRange = { from: number, to: number } | any[] | number | string

interface EmitterOptions {
    angle: PropertyRange
    speed: PropertyRange,
    rotation: PropertyRange,
    lifetime: PropertyRange,
    texture: PropertyRange,
    easing?: EasingFunction,
}

/**
 * A component for emitting a large amount of particles. Can be set up with 
 * EmitterOptions.
 */
export default class ParticleEmitter extends Component {
    particleContainer: PIXI.particles.ParticleContainer
    particles = new Set<DynamicParticle>()
    options: EmitterOptions
    selfDestructive: boolean
    started = false

    constructor (options: EmitterOptions, owner: Container, selfDestructive: boolean = false) {
        super(owner)
        this.options = options
        this.selfDestructive = selfDestructive
    }

    onInit () {
        this.particleContainer = new PIXI.particles.ParticleContainer(1000, {
            rotation: true
        })
        ;(<Container>this.owner).addChild(this.particleContainer)
    }
    
    onUpdate (deltaTime: number, elapsedTime: number) {
        // Update each created particle
        this.particles.forEach(particle => {
            const { x, y } = particle.object.position
            const vel = particle.velocity.multiply(particle.lifetime)
            const t = particle.t / particle.lifetime

            // Calculate easing
            const cX = particle.easing(t) * vel.x
            const cY = particle.easing(t) * vel.y

            // Update position & rotation
            particle.object.position.set(cX, cY)
            particle.object.rotation += particle.rotationalVelocity

            particle.t += deltaTime

            // Destroy the particle if its lifetime is over
            if (particle.t > particle.lifetime) { 
                particle.object.destroy()
                this.particles.delete(particle)
            }
        })

        // If emitter is selfDestructive, remove itself from the scene 
        // after all particles has disappeared.
        if (this.started && this.selfDestructive && this.particles.size == 0) {
            this.owner.destroy()
        }
    }

    /**
     * Burst a one-time shot of particles with certain amount.
     * @param amount Amount of particled
     */
    burst (amount: number) {
        this.started = true
        const texture = PIXI.Texture.from(this.applyProp(this.options.texture))

        for (let i = 0; i < amount; ++i) {
            const dparticle = this.createDynamicParticle(texture)

            this.particles.add(dparticle)
            this.particleContainer.addChild(dparticle.object)
        }
    }

    /**
     * Creates a single dynamic particle.
     * @param texture Texture of the particle
     */
    createDynamicParticle (texture: PIXI.Texture) : DynamicParticle {
        const { angle, speed, rotation, lifetime } = this.options

        const angleApplied = this.applyProp(angle) * Math.PI / 180
        const velocity = new Vec2(
            Math.sin(angleApplied), 
            Math.cos(angleApplied)).multiply(this.applyProp(speed))

        const particleParams = {
            velocity,
            rotationalVelocity: this.applyProp(rotation),
            lifetime: this.applyProp(lifetime),
            t: 0,
            easing: this.options.easing || EasingFunctions.Linear,
        }
            
        const sprite = new PIXI.Sprite(texture)
        sprite.anchor.set(0.5, 0.5)

        return { object: sprite, ...particleParams }
    }

    /**
     * Applies selectable property. If the property is constant, it returns
     * the constant. If it's a range, it returns a random value in the range.
     * If it's a list, it chooses a random item from the list.
     * @param property Property Range
     */
    applyProp (property: PropertyRange) {
        if (isNumber(property) || isString(property)) {
            return property
        }

        if (isArray(property)) {
            return property[Math.floor(Math.random() * property.length)]
        }

        const { from, to } = property
        return from + Math.random() * (to - from)
    }
}