import Component from '../engine/component'
import Message, { MessageAction } from '../engine/message'
import { Text, Sprite, Container } from '../engine/gameobject'
import GameCore from './gameCore'

function zeropad(number, amount) {
    if (number >= Math.pow(10, amount)) {
        return number;
    }
    return (Array(amount).join('0') + number).slice(-amount);
}

const heartSize = 24

/**
 * Component for display statusbar display on the top of the screen. It reads
 * the game state saved in GameCore component.
 */
export default class StatusbarDisplayComponent extends Component {
    private scoreDisplay: Text
    private gameCore: GameCore
    private livesDisplays: Sprite[] = []

    onInit () {
        this.gameCore = <GameCore>this.owner.scene.findGlobalComponentByClass('GameCore')

        // Student score display
        this.scoreDisplay = new Text(this.studentDisplay(), 'scoredisplay', this.owner.scene,
            { fill: 'white', font: '24px Press Start 2P', align: 'left' },
        )
        const app = this.owner.scene.application
        this.scoreDisplay.position.set(20, 20)
        this.scoreDisplay.anchor.set(0, 0)
        this.owner.addChildGameObject(this.scoreDisplay)

        // Student lives display
        const livesdisp = new Container('lives', this.owner.scene)
        for (let i = 0; i < 5; ++i) {
            const sprite = new Sprite(PIXI.Texture.from('heart'), 'heart', this.owner.scene)
            this.livesDisplays.push(sprite)
            sprite.width = heartSize
            sprite.height = heartSize
            sprite.x = i * heartSize
            livesdisp.addChild(sprite)
        }
        livesdisp.x = 225
        livesdisp.y = 16
        this.owner.scene.addObject(livesdisp)

        // Teacher score display
        const teacherText = new Text('SVECADAM\n99999999', 'teacherText', this.owner.scene,
            { fill: 'white', font: '24px Press Start 2P', align: 'right' },
        )
        teacherText.position.set(app.pixi.screen.width - 20, 20)
        teacherText.anchor.set(1, 0)
        this.owner.addChildGameObject(teacherText)

        // Versus text in the middle
        const vsText = new Text('VS', 'vsText', 
            this.owner.scene,
            { fill: 'white', font: '48px Press Start 2P', align: 'center' },
        )
        vsText.position.set(app.pixi.screen.width / 2, 20)
        vsText.anchor.set(0.5, 0)
        this.owner.addChildGameObject(vsText)

        // ...
        this.subscribe(MessageAction.STATE_CHANGED)
        this.updateDisplay()
    }

    studentDisplay () {
        return this.gameCore.state.studentName.toUpperCase() + '\n' + zeropad(this.gameCore.state.score, 8)
    }

    onMessage (msg: Message) {
        this.updateDisplay()
    }

    updateDisplay () {
        this.scoreDisplay.text = this.studentDisplay()

        for (let i = 0; i < 5; ++i) {
            this.livesDisplays[i].visible = this.gameCore.state.lives > i
        }
    }
}