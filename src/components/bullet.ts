import Component from '../engine/component'
import Message, { MessageAction } from '../engine/message'

/**
 * Components for moving a bullet and destroying itself after leaving the
 * viewport.
 */
export default class BulletComponent extends Component {
    private speed: number

    constructor (speed: number, owner) {
        super(owner)
        this.speed = speed
    }

    onUpdate (deltaTime: number, elapsedTime: number) {
        this.owner.y -= this.speed * deltaTime

        if (this.owner.y < 0) {
            this.owner.destroyObject()
        }
    }
}