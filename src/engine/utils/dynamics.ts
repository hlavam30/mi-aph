import Vec2 from './Vec2';

/**
 * Storage for aceleration and velocity 
 * @author Ing. Adam Vesecky
 * @see https://gitlab.fit.cvut.cz/svecadam/MI-APH/tree/master/ts/utils
 */
export default class Dynamics {
    friction: number;
    velocity: Vec2;

    constructor(velocity = new Vec2(0, 0), friction = 0){
        this.velocity = velocity;
        this.friction = friction;
    }

    applyFriction(delta: number) {
        this.velocity = this.velocity.multiply(1 / this.friction)
    }

    accelerate (acceleration: Vec2) {
        this.velocity.x += acceleration.x
        this.velocity.y += acceleration.y
    }
}
