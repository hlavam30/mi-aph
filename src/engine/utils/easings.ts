export type EasingFunction = (t: number, ..._rest) => number

export class EasingFunctions {
    static Linear: EasingFunction = (t: number): number => {
        return t
    }

    static EaseIn: EasingFunction = (t: number, factor: number): number => {
        return Math.pow(t, 4)
    }

    static EaseOut: EasingFunction = (t: number): number => {
        return Math.pow(t, 0.25)
    }

    static PreciseEaseIn: EasingFunction = (t: number): number => {
        return 1 - Math.sin(Math.PI / 2 * t + Math.PI / 2)
    }

    static PreciseEaseOut: EasingFunction = (t: number): number => {
        return Math.sin(Math.PI / 2 * t)
    }
}