import Vec2 from './vec2'
import * as PIXI from 'pixi.js'

export function upsertMapSet<K, V> (map: Map<K, Set<V>>, key: K, value: V) {
    if (map.has(key)) {
        map.get(key).add(value)
    } else {
        map.set(key, new Set([value]))
    }
}

export function angleLerp (fromAngle: number, toAngle: number, t: number) {
    const max = Math.PI * 2
    const delta = (toAngle - fromAngle) % max
    return fromAngle + (2 * delta % max - delta) * t
}

/**
 * @see https://gist.github.com/webinista/11240585#gistcomment-2363393
 * @author chadbr
 * @param arr Array to be chunked
 * @param chunkSize Chunk size
 */
export function chunk<T>(arr: Array<T>, chunkSize: number): Array<Array<T>> {
    return arr.reduce((prevVal: any, currVal: any, currIndx: number, array: Array<T>) =>
        !(currIndx % chunkSize) ?
        prevVal.concat([array.slice(currIndx, currIndx + chunkSize)]) :
        prevVal, []);
}

export function limitPositionToBoundaries (position: PIXI.Point, boundaries: PIXI.Rectangle, offset?: PIXI.Point) {
    if (!offset) offset = new PIXI.Point(0, 0)
    if (position.x > boundaries.width - offset.x) position.x = boundaries.width - offset.x
    if (position.x < offset.x) position.x = offset.x
    if (position.y > boundaries.height - offset.y) position.y = boundaries.height - offset.y
    if (position.y < offset.y) position.y = offset.y
}

export { default as Vec2 } from './vec2'
export { default as Dynamics } from './dynamics'
export { EasingFunctions, EasingFunction } from './easings'