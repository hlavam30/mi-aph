import GameObject from './gameobject'
import Message, { MessageAction } from './message'

let componentIdCounter = 0

/**
 * Component that defines a functional behavior of an entity which is attached to.
 */
export default class Component {
    // Auto-incremented unique component ID
	id = 0
	// Owner game object of this component
	owner: GameObject = null

	constructor (owner: GameObject) {
		this.id = componentIdCounter++
		this.owner = owner
	}

	/**
	 * Called when the component is being added to the scene.
	 */
	onInit () { }

	/**
	 * Handles incoming message.
	 * @param msg The message received
	 */
	onMessage (message: Message) { }

	/**
	 * Handles update loop.
	 */
	onUpdate (deltaTime: number, elapsedTime: number) { }

	/**
	 * Called before removal from scene.
	 */
	onRemove () { }

	/**
	 * Subscribes itself as a listener for action with given key
	 */
	subscribe (action: MessageAction, ...actions: MessageAction[]) {
		this.owner.scene.subscribeComponent(this, action)
		for (let action of actions) {
			this.owner.scene.subscribeComponent(this, action)
		}
	}

	/**
	 * Unsubscribes itself
	 */
	unsubscribe (action: MessageAction) {
		this.owner.scene.unsubscribeComponent(this, action)
	}

	/**
	 * Sends a message to all subscribers
	 */
	sendMessage (action: MessageAction, data: any = null) {
		this.owner.scene.distributeMessage({ action, sender: this, data })
	}
}


