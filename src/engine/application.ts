import * as PIXI from 'pixi.js'
import Scene from './scene'
import Stats from 'stats.js'

interface AppConfig {
  resolution: {
    width: number
    height: number
  },
  backgroundColor: number,
  showFPSCounter: boolean,
}

/**
 * Root application class
 */
export default class Application {
  pixi: PIXI.Application = null
  lastTime: number = 0
  ticker: PIXI.ticker.Ticker = null
  scene: Scene = null
  stats: Stats = new Stats()

  constructor (canvas: HTMLCanvasElement, config: AppConfig, scene?: Scene) {
    this.pixi = new PIXI.Application({
			view: canvas,
			backgroundColor: config.backgroundColor,
			width: config.resolution.width,
      height: config.resolution.height,
    })

    if (scene) {
      this.attachScene(scene)
    }

    // Initialize main ticker
    this.ticker = PIXI.ticker.shared
    this.ticker.autoStart = false
    this.ticker.stop()

    // Start game loop
    this.loop(0)

    // Display stats.js
    if (config.showFPSCounter) {
      this.stats.showPanel(0)
      document.body.append(this.stats.dom)
    }
  } 

  /**
   * Attaches a new scene to the application. The scene will be displayed.
   * @param scene Scene to be attached
   * @param destroyPrevious Destroy previous scene?
   */
  attachScene (scene: Scene, destroyPrevious = false) {
    if (this.scene) {
      this.pixi.stage.removeChild(this.scene.stage)

      if (destroyPrevious) {
        this.scene.destroy()
      }
    }
    this.pixi.stage.addChild(scene.stage)

    this.scene = scene
  }

  private loop (elapsedTime) {
    if (this.scene) {
      this.stats.begin()
      
      this.scene.update(this.ticker.deltaTime, elapsedTime)
      this.ticker.update()

      this.stats.end()  
    }
    requestAnimationFrame(time => this.loop(time))
  }
}