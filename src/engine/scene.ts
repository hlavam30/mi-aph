import Component from './component'
import GameObject, { Container } from './gameobject'
import Message, { MessageAction } from './message'
import { upsertMapSet } from './utils'
import Application from './application'
import { compare, cmp } from 'semver';

export default class Scene {
    stage: GameObject
    application: Application = null

    // Components subscribed to this scene
    private subscribedComponents: Map<MessageAction, Set<Component>>
    // Map of all registered objects
    private registeredObjects: { byId: Map<number, GameObject>, byTag: Map<string, Set<GameObject>> }

    constructor () {
        this.stage = new Container('_0scene', this)
        this.registeredObjects = {
            byId: new Map<number, GameObject>(),
            byTag: new Map<string, Set<GameObject>>(),
        }
        this.subscribedComponents = new Map<MessageAction, Set<Component>>()
    }

    addGlobalComponent (comp: Component) {
        this.stage.addComponent(comp)
    }

    findGlobalComponentByClass (name: string) : Component {
        return this.stage.findComponentByClass(name)
    }

    removeGlobalComponent (comp: Component) {
        this.stage.removeComponent(comp)
    }

    findObjectsByTag (tag: string) : GameObject[] {
        return this.registeredObjects.byTag.has(tag) ? 
            Array.from(this.registeredObjects.byTag.get(tag)) : []
    }

    addObject (gameObject: GameObject) {
        (<Container>this.stage).addChild(gameObject)
        this.registerObject(gameObject)
    }

    update (deltaTime: number, elapsedTime: number) {
        for (const obj of this.registeredObjects.byId.values()) {
            obj.update(deltaTime, elapsedTime)
        }

        this.stage.update(deltaTime, elapsedTime)
    }

    destroy () {
        this.registeredObjects.byId.clear()
        this.registeredObjects.byTag.clear()

        for (const cmpSet of this.subscribedComponents.values()) {
            for (const cmp of cmpSet.values()) {
                cmp.onRemove()
            }
        }

        for (const cmp of this.stage.components) {
            cmp.onRemove()
        }
    }

    registerObject (gameObject: GameObject) {
        this.registeredObjects.byId.set(gameObject.id, gameObject)
        upsertMapSet(this.registeredObjects.byTag, gameObject.tag, gameObject)
    }

    unregisterObject (gameObject: GameObject) {
        this.registeredObjects.byId.delete(gameObject.id)
        this.registeredObjects.byTag.get(gameObject.tag).delete(gameObject)
    }

    subscribeComponent (component: Component, action: MessageAction) {
        upsertMapSet(this.subscribedComponents, action, component)
    }

    unsubscribeComponent (component: Component, action: MessageAction) {
        if (action in this.subscribedComponents) {
            this.subscribedComponents.get(action).delete(component)
        }
    }

    distributeMessage (message: Message) {
        if (this.subscribedComponents.has(message.action)) {
            this.subscribedComponents.get(message.action).forEach((c: Component) => {
                c.onMessage(message)
            })
        }

        if (this.subscribedComponents.has(MessageAction.ANY)) {
            this.subscribedComponents.get(MessageAction.ANY).forEach((c: Component) => {
                c.onMessage(message)
            })
        }
    }
}