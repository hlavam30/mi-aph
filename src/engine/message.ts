import Component from './component'

export enum MessageAction {
    ANY = -1,
    NOTHING,
    AWAKE,
    DEBUG,
    GAME_OVER,
    BULLETS,
    SCORE,
    LIVES,
    STATE_CHANGED,
    BULLET_HIT,
    DIFFICULTY_CHANGED,
}

export default interface Message {
    action: MessageAction
    sender: Component
    data: any
}
