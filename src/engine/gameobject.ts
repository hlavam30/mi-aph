import Component from './Component'
import Scene from './Scene'
import * as PIXI from 'pixi.js'
import { removeAllListeners } from 'cluster';
import { Vec2 } from './utils'

type Constructable = new (...args: any[]) => object
let objectIdCounter = 0

/**
 * Enriches Constructable objects with componentual architecture.
 * @param base Class to be enriched
 */
function componentEnriched<T extends Constructable> (base: T) {
    class ComponentObject extends base {
        id = 0
        tag: string = null
        flags: string[] = []
        components: Component[] = []
        scene: Scene = null

        constructor (...params: any[]) {
            super(...params) 
            this.id = objectIdCounter++
        }

        componentConstructor (tag: string, scene: Scene) {
            this.tag = tag
            if (scene) this.scene = scene
        }

        addChildGameObject (obj: GameObject) {
            (<any>this).addChild(obj)
            obj.scene = this.scene
            this.scene.registerObject(obj)
        }

        /**
         * Attach new component to the game object. 
         * @param component Component to be added
         */
        addComponent (comp: Component) {
            comp.owner = <any>this
            this.components.push(comp)
            comp.onInit()
        }

        /**
         * Removes a component from the game object.
         * @param component Component to be removed
         */
        removeComponent (comp: Component) {
            comp.onRemove()
            this.components = this.components.filter(c => c !== comp)
        }

        /**
         * Removes a component from the game object by class name.
         * @param name Component class name
         */
        removeComponentByClass (name: string) {
            const comp = this.findComponentByClass(name)
            if (comp) {
                comp.onRemove()
                this.removeComponent(comp)
            }
        }

        /**
         * Removes all components from the game object.
         */
        removeAllComponents () {
            this.components.forEach(c => c.onRemove())
            this.components = []
        }

        /**
         * Tries to find a component by given class name.
         */
        findComponentByClass (name: string) {
            for (const comp of this.components) {
                if (comp.constructor.name === name) return comp
            }
            return null
        }

        /**
         * Adds flag to the game object.
         * @param flag Flag name
         */
        addFlag (flag: string) {
            this.flags.push(flag)
        }

        /**
         * Removes flag from the game object.
         * @param flag Flag name
         */
        removeFlag (flag: string) {
            this.flags = this.flags.filter(f => f !== flag)
        }

        /**
         * Returns true if game object has the flag.
         * @param flag Flag name
         */
        hasFlag (flag: string) : boolean {
            return this.flags.indexOf(flag) >= 0
        }

        /**
         * Updates object's components and all children.
         * @param deltaTime Time delta
         * @param elapsedTime Elapsed time
         */
        update (deltaTime: number, elapsedTime: number) {
            // Update all components
            this.components.forEach(cmp => cmp.onUpdate(deltaTime, elapsedTime))
        }

        /**
         * Destroys the object.
         */
        destroyObject () {
            this.removeAllComponents()
            this.scene.unregisterObject(<any>this)
            ;(<any>this).destroy()
        }

        /**
         * Moves the object by vector.
         * @param x Horizontal part of vector
         * @param y Vertical part of vector
         */
        moveBy (x: number, y: number) {
            (<any>this).x += x
            ;(<any>this).y += y
        }

        /**
         * Moves the obejct by vector.
         * @param vec Vector
         */
        moveByVec (vec: Vec2) {
            this.moveBy(vec.x, vec.y)
        }

        /**
         * Checks if two game objects are colliding.
         * @param other The other game object
         * @param radius Radius to be checked
         */
        checkCircleCollisionWith (other: GameObject, radius: number) {
            const { x, y } = (<any>this).position
            return new Vec2(other.x - x, other.y - y).magnitude() < radius
        }
    }

    return ComponentObject
}

/**
 * Game object componentual interface.
 */
export default interface GameObject extends PIXI.DisplayObject {
    id: number
    tag: string
    flags: string[]
    components: Component[]
    scene: Scene
    addChildGameObject (obj: GameObject)
    addComponent (comp: Component)
    removeComponent (comp: Component)
    removeComponentByClass (name: string)
    removeAllComponents ()
    findComponentByClass (name: string)
    addFlag (flag: string)
    removeFlag (flag: string)
    hasFlag (flag: string)
    update (deltaTime: number, elapsedTime: number)
    destroyObject ()
    moveBy (x: number, y: number)
    moveByVec (vec: Vec2)
    checkCircleCollisionWith (other: GameObject, radius: number)
}

/**
 * Container game object.
 */
export class Container extends componentEnriched(PIXI.Container) implements GameObject { 
    constructor (tag: string, scene: Scene) {
        super()
        this.componentConstructor(tag, scene)
    }
}

/**
 * Graphics game object.
 */
export class Graphics extends componentEnriched(PIXI.Graphics) implements GameObject { 
    constructor (tag: string, scene: Scene, nativeLines?: boolean) {
        super(nativeLines)
        this.componentConstructor(tag, scene)
    }
}

/**
 * Sprite game object.
 */
export class Sprite extends componentEnriched(PIXI.Sprite) implements GameObject { 
    constructor (texture: PIXI.Texture, tag: string, scene: Scene) {
        super(texture)
        this.componentConstructor(tag, scene)
    }
}

/**
 * Text game object.
 */
export class Text extends componentEnriched(PIXI.Text) implements GameObject { 
    constructor (text: string, tag: string, scene: Scene, style?: object | PIXI.TextStyle, canvas?: HTMLCanvasElement) {
        super(text, style, canvas)
        this.componentConstructor(tag, scene)
    }
}

/**
 * Returns next upcoming gameobject ID without advancing the counter
 */
export function nextIdWillBe () {
    return objectIdCounter
}