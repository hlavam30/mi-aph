export interface TimedEvent {
    fn: (number) => any
    at: number
    interval?: number
}

export default class Timer {
    private timedEvents = new Set<TimedEvent>()

    /**
     * Delays a call of function. Creates a timed event.
     * @param fn Function to be timed
     * @param at When the function should be called
     * @param repeat Repeat forever?
     */
    time (fn: (number) => any, at: number, repeat?: boolean) {
        const event = { fn, at, interval: repeat ? at : null }
        this.timedEvents.add(event)
        return event
    }

    /**
     * Cancels a timed event.
     * @param event Timed event
     */
    cancelTime (event: TimedEvent) {
        this.timedEvents.delete(event)
    }

    /**
     * Updates the timer. This should be called every loop.
     * @param elapsedTime Current elapsed time
     */
    update (elapsedTime: number) {
        for (const event of this.timedEvents) {
            if (elapsedTime > event.at) {
                event.fn(elapsedTime)
                if (event.interval) {
                    event.at = elapsedTime + event.interval
                } else {
                    this.timedEvents.delete(event)
                }
            }
        }
    }
}