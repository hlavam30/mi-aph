export interface Claim {
    id: number
    text: string
    correct: boolean
}

export interface ClaimsFile {
    meta: {
        version: number
        updated: string
        contributors: string[]
    }
    claims: Claim[]
}

/**
 * Claim loader is used for loading statements for the game from a JSON file.
 */
export default class ClaimLoader {
    private claims: ClaimsFile

    constructor () {
        this.claims = require('../assets/claims.json')
    }
     
    /**
     * Returns metadata of the file.
     */
    getMeta () {
        return this.claims.meta
    }

    /**
     * Returns the array of all claims in the file.
     */
    getAllClaims () {
        return this.claims.claims
    }

    /**
     * Returns one random claim from the loaded file.
     */
    getRandomClaim () {
        const id = Math.floor(Math.random() * this.claims.claims.length)
        return this.claims.claims[id]
    }
}