import Scene from './engine/scene'
import { Sprite, Container } from './engine/gameobject'
import * as Prefabs from './prefabFactory'
import Application from './engine/application'
import SoundManager from './soundManager'

import StatusbarDisplayComponent from './components/statusbarDisplay'
import GameCore from './components/gameCore'

/**
 * Main scene is a default game scene where the actual game happens.
 */
export default class MainScene extends Scene {
    constructor(app: Application, done: (MainScene) => void) {
        super()
        this.application = app

        //this.addGlobalComponent(new TalkativeComponent(this.stage)) // debug
        this.addGlobalComponent(new GameCore(this.stage))

        const statusbar = new Container('statusbar', this)
        this.addObject(statusbar)
        statusbar.addComponent(new StatusbarDisplayComponent(statusbar))

        Prefabs.makeTeacher(this)
        Prefabs.makePlayer(this)

        SoundManager.gameMusic.loop()

        done(this)
    }
}