import Scene from './engine/scene'
import { Text, Sprite } from './engine/gameobject'
import Application from './engine/application'
import KeyInputComponent, { KeyboardKey } from './components/keyInput'
import MainScene from './mainScene';

/**
 * Outro scene is displayed after the player lose.
 */
export default class OutroScene extends Scene {
    constructor(app: Application) {
        super()
        this.application = app
        const { width, height } = app.pixi.screen

        const background = new Sprite(PIXI.Texture.from('outro'), 'bkg', this)
        this.addObject(background)

        const gameoverText = new Text(
            'GAME OVER', 
            'gameovertext', this, 
            { fill: 'white', font: '72px Press Start 2P' }
        )

        gameoverText.anchor.set(0.5, 0.5)
        gameoverText.position.set(width / 2, height / 5)
        this.addObject(gameoverText)

        const rText = new Text(
            'PRESS [FIRE] TO RESTART', 
            'gameovertext', this, 
            { fill: 'white', font: '24px Press Start 2P' }
        )

        rText.anchor.set(0.5, 0.5)
        rText.position.set(width / 2, height / 5 + 60)
        this.addObject(rText)

        const keyInput = new KeyInputComponent(this.stage)
        this.addGlobalComponent(keyInput)

        keyInput.setListener(KeyboardKey.FIRE, () => {
            this.application.attachScene(new MainScene(this.application, () => {}), true)
        })
    }
}