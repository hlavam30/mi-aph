import Scene from './engine/scene'
import { Text, Graphics } from './engine/gameobject'
import Application from './engine/application'

/**
 * Load scene is displayed before the game assets are loaded.
 */
export default class LoadScene extends Scene {
    private progress: Graphics

    constructor(app: Application) {
        super()
        this.application = app
        const { width, height } = app.pixi.screen

        const loadText = new Text(
            'Loading APH Exam Simulator 2019...', 
            'loadtext', this, 
            { fill: 'white', font: '12px Arial' }
        )

        loadText.anchor.set(0.5, 0.5)
        loadText.position.set(width / 2, height / 2)
        this.addObject(loadText)

        const progress = new Graphics('progress', this)
        progress.position.set(width / 2, height / 2 + 24)
        this.addObject(progress)

        this.progress = progress

        this.updateProgress(20)
    }

    updateProgress (progress: number) {
        this.progress.clear()

        this.progress.beginFill(0x555555, 1)
        this.progress.drawRect(-100,0,200,3)
        this.progress.beginFill(0xffffff, 1)
        this.progress.drawRect(-100,0,progress * 2,3)
    }
}