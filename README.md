# ![APH Exam Simulator 2019](./assets/images/logo.png)

![Project Status](https://img.shields.io/badge/status-done-green.svg)

🕹 **Coursework game for MI-APH Course at FIT CTU in Prague, winter semester 2018/19**

The game can be played at http://aph.marianhlavac.cz

## About

In 'APH Exam Simulator 2019' the student, as the main protagonist, is making its way through valid and invalid statements to finish his exam successfully.

The game is written in TypeScript, using Pixi.js library for basic drawing in Canvas/WebGL.

## Screenshot

![Screenshot](./screenshot.png)

## Installation

Clone the repository and run

    yarn

After the packages are installed, run

    yarn start

And open `localhost:1234` in your browser.

## How to play

## Implemented Features

- **Componentual Architecture**
    - Can be seen in [component.ts](./src/engine/component.ts) and [gameobject.js](./src/engine/gameobject.ts).
- **Multiple Scenes (levels)**
    - There are 4 scenes: [LoadScene](./src/loadScene.ts), [IntroScene](./src/introScene.ts), [MainScene](./src/mainScene.ts), [OutroScene](./src/outroScene.ts).
- **Steering Behaviours**
    - There is some modified version of seek at [teacherBehaviour.ts:67](./src/components/teacherBehaviour.ts#L67) and bullet dodging at [teacherBehaviour.ts:89](./src/components/teacherBehaviour.ts#L89).
- **Particle System**
    - A simple particle system using `ParticleContainer` is implemented in component [emitterComponent.ts](./src/components/particleEmitter.ts).
    - The particle system is kinda flexible and its usage is demonstrated at [prefabFactory.ts:85](./src/prefabFactory.ts#L85).

But there is more, and also not all of these four mentioned features are 100%. See [the cool stuf to look at](#cool-stuff-to-look-at) in this README.

## Architecture Diagram

![Arch diagram](./diagram.png)

## Cool stuff to look at

The code has few hidden gems that are not apparent on the first sight.

- Fancy enriched PIXI components
    - There is no proxy class used.
    - There is a GameObject interface defined ([gameobject.ts:165](src/engine/gameobject.ts#L165)) that implements the component hierarchy methods.
    - Using multiple inheritance feature in TypeScript, there are redefined all basic display objects of PIXI at [gameobject.ts:190](src/engine/gameobject.ts#L190)) using `componentEnriched` special class and implementing the `GameObject` interface to support components.
    - Usage of this new classes are quite elegant:  
        ```ts
            import { Sprite } from './engine/gameobject'

            const player = new Sprite(PIXI.Texture.from('player'), 'player', scene)

            // Sprite (texture: PIXI.Texture, tag: string, scene: Scene) -> PIXI.DisplayObject
        ```
    - They're still extending the `PIXI.DisplayObject`, so they are completely compatible with the rest of PIXI functions.
- JSON loaded assets
    - All assets are automatically loaded from definitions in the [assets-catalog.json](./assets/assets-catalog.json) file.
- Dodging the bullet
    - The teacher charater tries to dodge the bullet, because he can't be killed!
- Options of the particle system
    - The particle system can have randomized values of position, angle, sprites and rotation. Each property can be constant, from random range or randomly selected from a list of options.
    - There are few easing functions implemented in [easings.ts](./src/engine/utils/easings.ts) that can be used for smoothing the position.
    - Example usage:
        ```ts
        const pemitter = new ParticleEmitter({
            angle: { from: 0, to: 360 }, // random angle 0 - 360 deg
            speed: 0.5, // constant speed of 0.5
            texture: ['debris1', 'debris2', 'debris3'], // random selection of texture
            ...
            easing: EasingFunctions.PreciseEaseOut, // easing function for positioning
        }, emitter, true)

        ```
- Dynamics with friction
    - Dynamics class in [dynamics.ts](./src/engine/utils/dynamics.ts) is modified with friction simulation

## Credits

Used music created by my friend [DDLBlue](https://soundcloud.com/ddl-blue) — Pavel Dohnal.  
For characted I've used [2D Characters Sprite Sheet](https://varenezeli.itch.io/characters-sprite-sheet) by [varenezeli](https://varenezeli.itch.io/) — David Jakubec.

Thanks, guys.

Sounds created with [Bfxr](http://bfxr.net).

## Known bugs

- Transition from IntroScene to MainScene has hardcoded parameters handover. When losing and going to OutroScene, the MainScene is without previous settings completely (student username missing).
- The bullet dodging, the teacher is doing, is not perfect.
- All timers are running when users focuses out the game.
- All timers are working solely with the `elapsedTime` variable.

and of course many more..

## What would be nice to have

- Nice graphics. The current aesthetics are meh.
- High scores at game over.
- The teacher could be smarter (only in the game ofc) and have some behaviour tree.
- Instead of letting the correct notes go, it would be better to pick them up.
- Revisit the exam statements and make sure they are 100% correct and useful.
- Fix the random selection of the statements the teacher throws. A list shuffle technique would be better.